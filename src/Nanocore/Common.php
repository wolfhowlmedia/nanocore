<?php
namespace Wolfhowlmedia\Nanocore;

/***
 * Skupna klasa.
 */
class Common {
	//redirekt na stran. Poslje se header 301 MOVED
	static public function redirect($location) {
		header('location: '.$location);
		die;
	}
	
	//Preveri se, ce je email, ki je vpisan v pravilnem formatu
	public static function check_email($email) {
		$pattern = 
			'/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])'.
			'(([a-z0-9-])*([a-z0-9]))+' . 
			'(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';
		return (bool)preg_match($pattern, $email);
	}
	
	//Ta in naslednja metoda zgenerirata ime, ki se jo uporabi za klicanje modula.
	public static function split_pagename ($page) {
		if (preg_match("/\s/", $page))
			return $page;           // Already split --- don't split any more.

		// FIXME: this algorithm is Anglo-centric.
		static $RE;
		if (!isset($RE)) {
			// This mess splits between a lower-case letter followed by either an upper-case
			// or a numeral; except that it wont split the prefixes 'Mc', 'De', or 'Di' off
			// of their tails.
			$RE[] = '/([[:lower:]])((?<!Mc|De|Di)[[:upper:]]|\d)/';
			// This the single-letter words 'I' and 'A' from any following capitalized words.
			$RE[] = '/(?: |^)([AI])([[:upper:]][[:lower:]])/';
			// Split numerals from following letters.
			$RE[] = '/(\d)([[:alpha:]])/';

			foreach ($RE as $key => $val)
				$RE[$key] = self::pcre_fix_posix_classes($val);
		}

		foreach ($RE as $regexp) {
			$page = preg_replace($regexp, '\\1 \\2', $page);
		}

		return $page;
	}


	private static function pcre_fix_posix_classes ($regexp) {
		// First check to see if our PCRE lib supports POSIX character
		// classes.  If it does, there's nothing to do.
		if (preg_match('/[[:upper:]]/', 'A'))
			return $regexp;

		static $classes = array(
					'alnum' => "0-9A-Za-z\xc0-\xd6\xd8-\xf6\xf8-\xff",
					'alpha' => "A-Za-z\xc0-\xd6\xd8-\xf6\xf8-\xff",
					'upper' => "A-Z\xc0-\xd6\xd8-\xde",
					'lower' => "a-z\xdf-\xf6\xf8-\xff"
					);

		$keys = join('|', array_keys($classes));

		return preg_replace("/\[:($keys):]/e", '$classes["\1"]', $regexp);
	}
	
	/**
	* Generates an UUID
	* Remarks from Primoz: The original generator doesn't generate standard UUID4
	*
	* @author     Anis uddin Ahmad <admin@ajaxray.com>
	* @author     Primoz Anzur <stormchaser1@gmail.com> 
	* @return     string  the formatted uuid
	*/
	static public function uuid4() {
		$chars = sha1(uniqid(mt_rand(), true));
		$ystr = array('8', '9', 'a','b');

		$uuid  = substr($chars,0,8) . '-';
		$uuid .= substr($chars,8,4) . '-';
		$uuid .= 'a'.substr($chars,12,3) . '-';
		$uuid .= $ystr[mt_rand(0, 3)].substr($chars,16,3) . '-';
		$uuid .= substr($chars,20,12);
		return $uuid;
	}

}
