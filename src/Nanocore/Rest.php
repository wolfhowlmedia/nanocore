<?php
namespace Wolfhowlmedia\Nanocore;
use App\Controllers;
use Exceptions;

class Rest {
	public static function get_method() {
		return trim(strtolower($_SERVER['REQUEST_METHOD']));
	}

	public static function __callStatic($name, $arguments) {
		if (self::get_method() == $name) {
			if (sizeof($arguments) < 2) { //we need at least 2 args: Name of the class and name of the method 
				throw new Exceptions\RestInsufficientArgumentsException('Insufficient arguments for REST call!');
			}
			$classname = 'App\\Controllers\\'.$arguments[0];
			$method    = $arguments[1];
			return call_user_func_array([new $classname, $method], array_slice($arguments, 2));
		}
	}
}
