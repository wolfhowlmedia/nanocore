<?php
namespace Wolfhowlmedia\Nanocore\Request;
//POST class. Same as GET with a difference that it looks into _POST
class Post {
	static function ret($param, $detect = false, $nullout = false) {
		if ($detect) {
			if (isset($_POST[$param])) {
				return $_POST[$param];
			}
			return $nullout;
		} else {
			if (isset($_POST[$param])) {
				return $_POST[$param];
			}
			return $nullout;
		}
	}

	static function is_set($param, $choice = array()) {
		if (!empty($choice)) {
			if (isset($_POST[$param])) {
				return $choice[0];
			}
			return $choice[1];
		} else {
			if (isset($_POST[$param])) {
				return true;
			}
			return false;
		}
	}

	static function retall() {
		return $_POST;
	}
}

