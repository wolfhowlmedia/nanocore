<?php
namespace Wolfhowlmedia\Nanocore\Request;

class Request {
	//Gather information
	static function ret($param, $detect = false, $nullout = false) {
		if ($detect) {
			if (isset($_REQUEST[$param])) {
				return $_REQUEST[$param];
			}
			return $nullout;
		} else {
			if (isset($_REQUEST[$param])) {
				return $_REQUEST[$param];
			}
			return $nullout;
		}
	}

	//Is index set?
	static function is_set($param) {
		if (isset($_REQUEST[$param])) {
			return true;
		}
		return false;
	}

	//We get everything back...
	static function retall() {
		return $_REQUEST;
	}
}
