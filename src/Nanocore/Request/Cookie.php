<?php
namespace Wolfhowlmedia\Nanocore\Request;
//Cookies class.
class Cookie {
	static function ret($param, $nullout = false) {
		if (isset($_COOKIE[$param])) {
			return $_COOKIE[$param];
		}
		return $nullout;
	}

	static function is_set($param, $choice = array()) {
		if (!empty($choice)) {
			if (isset($_COOKIE[$param])) {
				return $choice[0];
			}
			return $choice[1];
		} else {
			if (isset($_COOKIE[$param])) {
				return true;
			}
			return false;
		}
	}
}
