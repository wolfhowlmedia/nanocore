<?php
namespace Wolfhowlmedia\Nanocore\Request;

class Get {
	//Gather information
	static function ret($param, $detect = false, $nullout = false) {
		if ($detect) {
			if (isset($_GET[$param])) {
				return $_GET[$param];
			}
			return $nullout;
		} else {
			if (isset($_GET[$param])) {
				return $_GET[$param];
			}
			return $nullout;
		}
	}

	//Is index set?
	static function is_set($param, $choice = array()) {
		if (!empty($choice)) {
			if (isset($_GET[$param])) {
				return $choice[0];
			}
			return $choice[1];
		} else {
			if (isset($_GET[$param])) {
				return $_GET[$param];
			}
			return false;
		}
	}

	//We get everything back...
	static function retall() {
		return $_GET;
	}
}
