<?php
namespace Wolfhowlmedia\Nanocore;

use App\Controllers;

class Resolver {
	private $path = '';
	
	public function __construct() {
		//If the path is only index.php, then we assume the path is /
		$req = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
		$this->path = explode('?', $req, 2)[0];
	}
	
	//We get path in an array and all that good stuff
	public function get_path_params() {
		$params = explode('/', ltrim($this->path, '/'));
		array_shift($params);
		return $params;
	}
	
	//Render the module
	public function render_page() {
		$tmppath = '/' . ltrim($this->path, '/');
		if (!isset($tmppath)) {
			$tmppath[1] = '';
		}
		return $this->resolve_request($tmppath);
	}
	
	//We load the information from the class/method
	private function resolve_request($url) {
		//load the information from the URL and request method

		$url = trim($url, '/');

		$method       = Rest::get_method();
		$paths        = Route::preload($method);
		$p_split      = explode('/', $url);
		$p_split_size = sizeof($p_split);
		
		foreach ($paths['urls'] as $k => $v) {
			$key_split = explode('/', $k);
			if ($p_split_size == sizeof($key_split)) {
				$m = preg_match('#'.$k.'#', $url, $matches);
				print_r($matches);
				echo "beep";
				print_r($paths['args'][$k]);
				echo "beep";
				print_r($key_split);
				echo "beep";
				print_r(array_search('([a-zA-Z0-9]+)', $key_split));
				echo "beep";
			}
			/*
			if($p == $url) {
				$_call = explode('::', $v);
				//return Rest::$method($_call[0], $_call[1], $url);
			}
			*/
		}
	}
}
