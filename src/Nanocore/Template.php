<?php
namespace Wolfhowlmedia\Nanocore;

//Template rendering class
class Template {
	private $out = '';
	private $_data = array();

	//Load template
	public function __construct($file, $module_data = null) {
		$this->templatefile = $file;
		$this->module_data = $module_data;
	}

	//Cleanup HTML
	static public function html($input) {
		return htmlspecialchars($input, ENT_QUOTES, 'UTF-8');
	}

	//Cleanup URL
	static public function url($input) {
		return urlencode($input);
	}

	//Add non-standard data
	public function set_data($key, $value) {
		$this->_data[$key] = $value;
	}

	private function data($key, $default = []) {
		if (!isset($this->_data[$key])) {
			return $default;
		} else {
			return $this->_data[$key];
		}
	}

	//Render template or return 404
	public function render() {
		if (file_exists(TEMPLATE_PATH . $this->templatefile)) {
			ob_start();
			include TEMPLATE_PATH . $this->templatefile;
			$this->out = ob_get_clean();
		} else {
			return 'File: '.TEMPLATE_PATH . $this->templatefile . ' doesn\'t exist';
		}

		return $this->out;
	}
}
