<?php

namespace Wolfhowlmedia\Nanocore\Exceptions;

class RestInsufficientArgumentsException extends \Exception {}
