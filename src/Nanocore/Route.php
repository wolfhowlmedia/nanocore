<?php
namespace Wolfhowlmedia\Nanocore;

class Route {
	private static $urls = [];
	private static $args = [];

	public static function __callStatic($name, $arguments) {
		$request_method = Rest::get_method();

		foreach((array)$arguments[0] as $arg) {
			preg_match_all('#\[([a-zA-Z0-9]+)\]#', $arg, $matched_args);
			$arg = preg_replace('#(\[[a-zA-Z0-9]+\])#', '([a-zA-Z0-9]+)', $arg);

			self::$urls[$name][trim($arg, '/')] = (string)$arguments[1];
			self::$args[$name][trim($arg, '/')] = $matched_args[1];
		}
	}

	public static function preload($method = null) {
		if (is_null($method)) {
			return [
				'urls' => (array)self::$urls,
				'args' => (array)self::$args,
			];
		} else {
			if(isset(self::$urls[$method])) {
				return [
					'urls' => (array)self::$urls[$method],
					'args' => (array)self::$args[$method],
				];
			}
		}

		return [];
	}
}
